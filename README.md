# Gemeindeverband Horn Abfalltermin scraper

Hole die Abfall-Abholtermine vom Gemeindeverband Horn (GVH) https://horn.umweltverbaende.at/?kat=32

Das Projekt wurde gestartet für die Gemeinde Sigmundsherberg mit der URL: https://horn.umweltverbaende.at/?gem_nr=31124&jahr=2023&kat=32

Inzwischen sind ein paar andere Gemeinden hinzu gekommen.

Das Projekt konvertiert die Abholtermine in eine iCalendar formatierte `.ics` Datei um diese dann einfach am Handy oder Thunderbird zur Verfügung zu haben.

Links zu den Kalendern:

- Horn - Sigmundsherberg:   [gvh_abholtermine_sigmundsherberg.ics](https://neroburner.gitlab.io/gvh_abholtermine_to_ics/gvh_abholtermine_sigmundsherberg.ics)
- Horn - Weitersfeld:       [gvh_abholtermine_weitersfeld.ics](https://neroburner.gitlab.io/gvh_abholtermine_to_ics/gvh_abholtermine_weitersfeld.ics)
- Hollabrunn - Zellerndorf: [gvh_abholtermine_zellerndorf.ics](https://neroburner.gitlab.io/gvh_abholtermine_to_ics/gvh_abholtermine_zellerndorf.ics)
