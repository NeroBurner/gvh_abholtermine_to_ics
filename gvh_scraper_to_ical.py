#!/usr/bin/env python3
import argparse
import json
import re
import requests
import os
from datetime import datetime

def scrape_site(url="https://horn.umweltverbaende.at/?gem_nr=31124&jahr=2023&kat=32"):
    """scrape gvh site for dates"""
    r = requests.get(url)
    html = r.text

    # write or load local cache file, useful for debugging
    #with open("site.html", "w", encoding="utf-8") as f:
    #    f.write(html)
    #with open("site.html", "r", encoding="utf-8") as f:
    #    html = f.read()

    # get entries from site looking as follows:
    # <div class="tunterlegt">
    #   FR &nbsp; 05.05.2023 &nbsp; Biotonne
    # </div>
    # or as follows
    # https://pypi.org/project/camelot-py/
    # <div class="tunterlegt">
    #   MO &nbsp; 22.05.2023 &nbsp; Restmüll
    # </div>
    re_elements = '(?:<div class="tunterlegt">)(.*?)(?:</div>)'
    matches = re.findall(re_elements, html, re.DOTALL)

    def parse_entry(entry_html : str):
        """extract date and type garbage from the product html"""
        m = re.search('&nbsp; (\d\d[.]\d\d[.]\d\d\d\d) &nbsp; ([^\\n]+)\n', entry_html)
        date_str = m[1]
        garbage_str = m[2]

        ret = {
            "date_str": date_str,
            "type": garbage_str,
        }
        return ret

    entries = [parse_entry(entry) for entry in matches]
    return entries

def to_ical(entries, url):
    """format the gargabe entries to an ical text format"""

    # add header
    ret = """BEGIN:VCALENDAR
VERSION:2.0
PRODID:-{url}
CALSCALE:GREGORIAN
""".format(url=url)

    # add vevent entries
    def to_ical_entry(entry):
        """per entry create a new whole day event"""
        date_str = entry["date_str"]
        timestamp = datetime.strptime(date_str, "%d.%m.%Y")
        garbage_str = entry["type"]

        uid="{}.{}@gvh.at".format(date_str, garbage_str[:1])

        vevent = """BEGIN:VEVENT
UID:{uid}
SUMMARY:{garbage_str}
DTSTART;VALUE=DATE:{date_formated}
END:VEVENT""".format(
            uid=uid,
            date_str=date_str,
            garbage_str=garbage_str,
            date_formated=timestamp.strftime("%Y%m%d"),
        )
        return vevent

    ret += "\n".join([to_ical_entry(entry) for entry in entries])
    # add footer
    ret += "\nEND:VCALENDAR"
    return ret

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--url",
                        help="URL to Abfallverband site to scrape garbage collection dates.",
                        default="https://horn.umweltverbaende.at/?gem_nr=31124&kat=32") # default to Sigmundsherberg (Horn)
    parser.add_argument("-o", "--output",
                        help="ICS output file",
                        default="public/gvh_abholtermine_sigmundsherberg.ics")
    args = parser.parse_args()

    print("reading garbage collection dates from URL: {}".format(args.url))
    entries = scrape_site(url=args.url)
    # write or load local cache file, useful for debugging
    #with open("entries.json", "w", encoding="utf-8") as f:
    #    json.dump(entries, f, indent=2, ensure_ascii=False)
    #with open("products.json", "r", encoding="utf-8") as f:
    #    products = json.load(f)
    print("collected {:d} collection entries".format(len(entries)))

    print("converting collected entries to iCalendar format")
    project_url="https://gitlab.com/NeroBurner/gvh_abholtermine_to_ics"
    ical_str = to_ical(entries, project_url)

    if not os.path.exists("public"):
        os.mkdir("public")
    print("writing ics calendar to file: {}".format(args.output))
    with open(args.output, "w", encoding="utf-8") as f:
        f.write(ical_str)

    print("Finished!")

if __name__ == "__main__":
    main()
